package br.com.apitest;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.Gson;

import br.com.utils.JsonFile;
import br.com.utils.JsonRead;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CepTest {

	private static final String BASE_URL = "https://viacep.com.br/ws/";
	static JsonRead file = new JsonRead();

	@Test
	public static void cepValido() throws IOException {
		RestAssured.baseURI = BASE_URL;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "/91060900/json/");

		String responseBody = response.getBody().asString();
		byte[] bytes = responseBody.getBytes(StandardCharsets.UTF_8);
		String novoretorno = new String(bytes, StandardCharsets.UTF_8);

		Gson gson = new Gson();
		JsonFile responseResult = gson.fromJson(novoretorno, JsonFile.class);
		String rt = responseResult.toString();
		String retornoJson = JsonRead.readJson("./retorno1.json");
		Assert.assertEquals(rt, retornoJson);
	
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200); 
		String statusLine = response.getStatusLine();
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");

	}

	@Test
	public void cepInexistente() {
		RestAssured.baseURI = BASE_URL;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "/90000000/json/");
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		String statusLine = response.getStatusLine();
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	}

	@Test
	public void cepInvalido() {
		RestAssured.baseURI = BASE_URL;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "/0000000/json/");
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 400);
		String statusLine = response.getStatusLine();
		Assert.assertEquals(statusLine, "HTTP/1.1 400 Bad Request");
	}

}
