package br.com.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import com.google.gson.Gson;

public class JsonRead {

	public static String readJson(String json) throws IOException {

		InputStream inputStream = JsonFile.class.getClassLoader()
				.getResourceAsStream(json);
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

		Gson gson = new Gson();
		JsonFile jsonResult = gson.fromJson(br, JsonFile.class);
		
		return jsonResult.toString();
	}

}